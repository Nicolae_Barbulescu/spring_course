package com.course.springdemo;

public class MyApp {
	
	public static void main(String[] args) {
		// create the object
		Coach theCouch = new TrackCoach(null);
		
		// use the object
		System.out.println(theCouch.getDailyWorkout());
	}

}

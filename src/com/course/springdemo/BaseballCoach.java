package com.course.springdemo;

public class BaseballCoach implements Coach{
	// define a private field for the dependency
	private FortuneService fortuneService;
	
	// define a constructor for dependency injection
	
	public BaseballCoach(FortuneService theFortuneService) {
		fortuneService = theFortuneService;
	}
	
	@Override
	public String getDailyWorkout() {
		return "100 push-ups, 100 squats, and a 10 kilometer run";
	}

	@Override
	public String getDailyFortune() {
		return fortuneService.getFortune();
	}
}

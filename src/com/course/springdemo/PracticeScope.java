package com.course.springdemo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class PracticeScope {

	public static void main(String[] args) {
		ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext("applicationContextScopePractice.xml");
		Coach theCoach = context.getBean("myCoach", BaseballCoach.class);
		
		Coach otherCoach = context.getBean("myCoach", BaseballCoach.class);
		boolean isEqual = (theCoach == otherCoach);
		
		System.out.println("They are equal: " + isEqual);
		

	}

}

package com.course.springdemo;

public class CircuitTrainingCoach implements Coach{
	private FortuneService fortuneService;
	
	public CircuitTrainingCoach(FortuneService theFortuneService) {
		fortuneService = theFortuneService;
	}
	@Override
	public String getDailyWorkout() {
		return "do 10 push-ups, run 3 laps";
	}

	@Override
	public String getDailyFortune() {
		return fortuneService.getFortune();
	}
}

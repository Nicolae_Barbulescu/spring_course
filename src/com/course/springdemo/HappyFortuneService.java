package com.course.springdemo;

import java.util.Random;

public class HappyFortuneService implements FortuneService {
	private Random rand = new Random();
	private static final String fortunes[] = new String[] {
			"This is a good day", "This is a bad day", "Go back to monkey"};
	
	@Override
	public String getFortune() {
		return fortunes[rand.nextInt(fortunes.length)];
	}
	
}
